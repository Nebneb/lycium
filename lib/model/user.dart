class User {

  User(this.firstname, this.lastname, this.email, [this.pictureUrl, this.largePictureUrl, this.city, this.state]);

  String firstname;
  String lastname;
  String email;
  String? pictureUrl;
  String? largePictureUrl;
  String? city;
  String? state;

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      json["name"]["first"],
      json["name"]["last"],
      json["email"],
      json["picture"]["medium"],
      json["picture"]["large"],
      json["location"]["city"],
      json["location"]["state"]
    );
  }
}
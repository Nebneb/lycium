import 'package:flutter/material.dart';
import 'package:lycium/config.dart';

import 'pages/home_page.dart';

void main() {
  runApp(MainApp(Config.defaultConfiguration));
}

class MainApp extends StatelessWidget {
  const MainApp(this.configuration, {Key? key}) : super(key: key);
  final Config configuration;

  @override
  Widget build(BuildContext context) {
    // Setting the configuration Handler to make it available
    // for the whole MaterialApp component. The configuration
    // is also required for Widgets tests.
    return ConfigHandler(
      config: configuration,
      child: const MaterialApp(
        home: HomePage(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

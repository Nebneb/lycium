import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:lycium/data/user_provider_interface.dart';
import 'package:lycium/model/user.dart';

class UserRepository extends ChangeNotifier {
  UserRepository(this.userDataProvider);

  UserProviderInterface userDataProvider;
  final List<User> _users = [];
  final int pagination = 20;

  /// Get the already fetched users. It can be empty if no page
  /// was fetched before.
  UnmodifiableListView<User> get users => UnmodifiableListView(_users);

  /// This shall load the given page of users
  /// and add the result to the list.
  Future<void> fetchUsers(int page) async {
    // print("fetchingUsers($page)");
    final result = await userDataProvider.fetchUsers(page, pagination);
    _users.addAll(result.toList());
    notifyListeners();
  }
}

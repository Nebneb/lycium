import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lycium/config.dart';

class ProfilePicture extends StatelessWidget {
  final String userName;
  final String? pictureUrl;
  final double radius;

  const ProfilePicture(this.userName, this.pictureUrl, this.radius, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final allowNetwork = ConfigHandler.of(context).config.allowNetwork;

    if (pictureUrl != null && allowNetwork) {
      return CircleAvatar(
        backgroundImage: NetworkImage(pictureUrl!),
        radius: radius,
      );
    }
    return CircleAvatar(
      backgroundColor: Colors.blue,
      child: Text(
        userName.substring(0, 1),
        style: const TextStyle(
          fontSize: 24.0,
          color: Colors.white,
        ),
      ),
      radius: radius,
    );
  }
}

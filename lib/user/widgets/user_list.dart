import 'package:flutter/material.dart';
import 'package:lycium/user/user_list_view_model.dart';
import 'package:lycium/user/widgets/user_list_item.dart';
import 'package:provider/provider.dart';

class UserList extends StatefulWidget {
  const UserList({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  ScrollController scrollController = ScrollController();
  bool hasReachedBottom = true; // initialized to true to have initial loader

  @override
  Widget build(BuildContext context) {
    // the viewModel is a ChangeNotifier. It only calls
    // notifyListeners when fetching a new bunch of users.
    var userListViewModel = context.watch<UserListViewModel>();

    return Stack(
      alignment: AlignmentDirectional.bottomCenter,
      children: [
        ListView.separated(
          itemCount: userListViewModel.itemCount,
          separatorBuilder: (_, __) => const Divider(),
          padding: const EdgeInsets.only(bottom: 0.0),
          itemBuilder: (context, index) {
            return UserListItem(userListViewModel.getItem(index));
          },
          controller: scrollController,
        ),
        if (userListViewModel.isLoading && hasReachedBottom)
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100.0),
            ),
            elevation: 2,
            child: Container(
              child: const CircularProgressIndicator(),
              padding: const EdgeInsets.all(8.0),
            ),
            margin: const EdgeInsets.only(bottom: 24.0),
          ),
      ],
    );
  }

  @override
  void initState() {
    // We can get the user repository to get initial data from context provider
    var userListViewModel = Provider.of<UserListViewModel>(context, listen: false);

    // Scroll listener
    scrollController.addListener(() {
      // If bottom has been reached, set state to display loader if needed
      if ((scrollController.position.pixels == scrollController.position.maxScrollExtent)) {
        setState(() {
          hasReachedBottom = true;
        });
      } else if (hasReachedBottom) {
        // setState only if hasReachedBottom is true to avoid rebuild on every scroll event
        setState(() {
          hasReachedBottom = false;
        });
      }

      // Check if scroll is 400 pixels at most from the bottom of the list view to
      // trigger loadNextPage before reaching the bottom. (Pre-fetch)
      if (scrollController.position.pixels > scrollController.position.maxScrollExtent - 400
          && !userListViewModel.isLoading) {
        // As this impacts the repository, and as the repository is a ChangeNotifier,
        // There is no need to setState to update the users as it is already bound.
        // print("loading next page...");
        userListViewModel.loadNextPage();
      }
    });
    super.initState();

    userListViewModel.loadInitialData();
  }
}

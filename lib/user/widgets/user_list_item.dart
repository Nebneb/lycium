import 'package:flutter/material.dart';
import 'package:lycium/model/user.dart';
import 'package:lycium/pages/user_profile.dart';
import 'package:lycium/user/user_view_model.dart';
import 'package:lycium/user/widgets/user_picture.dart';

class UserListItem extends StatelessWidget {
  const UserListItem(this.user, {Key? key}) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    var userVM = UserViewModel(user);

    return ListTile(
      leading: ProfilePicture(user.firstname, user.pictureUrl, 30),
      title: Text(userVM.fullName),
      contentPadding: const EdgeInsets.symmetric(
        vertical: 4.0,
        horizontal: 16.0,
      ),
      onTap: () => {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => UserProfile(user)))
      },
    );
  }
}

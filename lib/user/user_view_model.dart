import 'package:lycium/model/user.dart';
import 'package:url_launcher/url_launcher.dart';

class UserViewModel {
  final User user;

  UserViewModel(this.user);

  String get fullName {
    return "${user.firstname} ${user.lastname}";
  }

  String get firstname {
    return user.firstname;
  }

  String get lastname {
    return user.lastname;
  }

  String get email {
    return user.email;
  }

  String get address {
    if (user.city == null && user.state == null) return "Unknown";
    return "${user.city}, ${user.state}";
  }

  void sendEmail() async {
    String mailUrl = "mailto:${user.email}";
    try {
      await launch(mailUrl);
    } catch (e) {
      //print(e.toString());
    }
  }
}

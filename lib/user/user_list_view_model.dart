import 'package:flutter/material.dart';
import 'package:lycium/model/user.dart';
import 'package:lycium/repository/user_repository.dart';

class UserListViewModel extends ChangeNotifier {
  UserListViewModel(this.userRepository);

  UserRepository userRepository;
  int page = -1;
  bool isLoading = false;

  Future<void> fetchUsers(int page, bool shouldNotify) async {
    isLoading = true;
    if (shouldNotify) notifyListeners();
    await userRepository.fetchUsers(page);
    isLoading = false;
    notifyListeners();
  }

  get itemCount {
    return userRepository.users.length;
  }

  User getItem(int listIndex) {
    return userRepository.users[listIndex];
  }

  /// Fetch the first page (0) of users.
  /// This will not notify listeners when changing
  /// isLoading to true, to avoid rebuilding widget
  /// in the initial state.
  void loadInitialData() {
    page = 0;
    fetchUsers(page, false);
  }

  void loadNextPage() {
    page += 1;
    fetchUsers(page, true);
  }
}

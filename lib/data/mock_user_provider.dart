import 'dart:math';

import 'package:lycium/data/user_provider_interface.dart';
import 'package:lycium/model/user.dart';

class MockUserProvider extends UserProviderInterface {
  final List<User> users = [
    User("Harry", "Potter", "quidditch.ftw@example.com", "https://static.hitek.fr/img/up_m/1521184988/harrypotter1.jpg",
        "https://static.hitek.fr/img/up_m/1521184988/harrypotter1.jpg", "England", "Little Whinging"),
    User(
      "Ronald",
      "Weasley",
      "ron@weasley.com",
      "https://media.melty.fr/article-4329688-social-f12/media.jpg",
      "https://media.melty.fr/article-4329688-social-f12/media.jpg",
    ),
    User(
      "Hermione",
      "Granger",
      "hermione.granger@gmail.com",
      null,
      "https://lecahier.com/wp-content/uploads/2017/11/hermione.jpg"
    ),
    User(
        "Tom",
        "Jedusor",
        "i.am.lord.voldemort@hotmail.muggle",
        "https://img.wattpad.com/a955e3dda65cb7fc0aa6479200e446b7cb4f7741/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f776174747061642d6d656469612d736572766963652f53746f7279496d6167652f494e514c477850784e56386879773d3d2d3839373534383132392e313631353232613465306166626138333430343438303034363031382e6a7067?s=fit&w=720&h=720",
        "https://static.wikia.nocookie.net/harrypotter/images/b/b9/Lordvoldemort-1-.jpg/revision/latest/top-crop/width/360/height/450?cb=20140312195432&path-prefix=fr",
        "London",
        "England"),
  ];

  User getRandomUser() {
    int randIndex = Random().nextInt(users.length);
    return users[randIndex];
  }

  List<User> getUsers([int pageIndex = 0]) {
    users.shuffle();
    return users;
  }

  @override
  Future<List<User>> fetchUsers([int pageIndex = 0, int itemPerPage = 20]) async {
    return getUsers(pageIndex);
  }
}

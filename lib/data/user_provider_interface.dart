import 'package:lycium/model/user.dart';

abstract class UserProviderInterface {
  /// Returns a Future<List<User>> of the asked page.
  /// The function will throw an error if something
  /// goes wrong while fetching users.
  Future<List<User>> fetchUsers([int pageIndex = 0, int itemPerPage = 20]);
}
import 'dart:convert';

import 'package:lycium/data/user_provider_interface.dart';
import 'package:lycium/model/user.dart';
import 'package:http/http.dart' as http;

class RemoteUserProvider extends UserProviderInterface {

  final String baseUrl = "https://randomuser.me/api/";
  final String includeFields = "inc=name,email,picture,location";

  @override
  Future<List<User>> fetchUsers([int pageIndex = 0, int itemPerPage = 20]) async {
    final url = "$baseUrl?page=${pageIndex.toString()}&results=$itemPerPage&seed=abc&$includeFields";
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200) {
      final body = jsonDecode(response.body);
      final Iterable json = body["results"];
      return json.map((userJson) => User.fromJson(userJson)).toList();
    } else {
      throw Exception("Error while fetching user [page : $pageIndex]");
    }
  }

}
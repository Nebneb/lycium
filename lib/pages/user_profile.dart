import 'package:flutter/material.dart';
import 'package:lycium/model/user.dart';
import 'package:lycium/user/user_view_model.dart';
import 'package:lycium/user/widgets/user_picture.dart';

class UserProfile extends StatelessWidget {
  const UserProfile(this.user, {Key? key}) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    UserViewModel userVM = UserViewModel(user);

    return Scaffold(
        appBar: AppBar(
          title: const Text("Profile"),
        ),
        body: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 12.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // === PROFILE PICTURE ===
              SizedBox(
                width: 150,
                height: 150,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0),
                  ),
                  elevation: 2,
                  child: ProfilePicture(user.firstname, user.largePictureUrl, 50),
                ),
              ),
              // === FULL NAME ===
              Container(
                margin: const EdgeInsets.only(top: 16, bottom: 32),
                child: Text(
                  userVM.fullName,
                  style: const TextStyle(fontSize: 25.0, color: Colors.blueGrey, letterSpacing: 2.0, fontWeight: FontWeight.w400),
                ),
              ),
              // === OTHER DETAILS ===
              Card(
                  elevation: 6,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.symmetric(
                          vertical: 26.0,
                          horizontal: 16.0,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ProfileInfoText("Email: ${userVM.email}"),
                            Container(
                              margin: const EdgeInsets.symmetric(vertical: 8),
                              child: const Divider(),
                            ),
                            ProfileInfoText("Location: ${userVM.address}"),
                          ],
                        ),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () {
                              userVM.sendEmail();
                            },
                            child: const Text("SEND EMAIL"),
                          ),
                        ],
                      )
                    ],
                  )),
            ],
          ),
        ));
  }
}

class ProfileInfoText extends StatelessWidget {
  const ProfileInfoText(this.text, {Key? key}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 25,
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 16.0,
          color: Colors.black87,
        ),
      ),
    );
  }
}

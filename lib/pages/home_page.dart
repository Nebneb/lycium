import 'package:flutter/material.dart';
import 'package:lycium/config.dart';
import 'package:lycium/data/mock_user_provider.dart';
import 'package:lycium/data/remote_user_provider.dart';
import 'package:lycium/repository/user_repository.dart';
import 'package:lycium/user/user_list_view_model.dart';
import 'package:provider/provider.dart';

import '../user/widgets/user_list.dart';

// HomePage is at the moment unnecessarily a StatefulWidget because it
// doesn't have any state change. Keeping it Stateful should only be
// useful if we improve this app with some changes on the home page,
// which is most likely something that would happen later on.
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {

    final config = ConfigHandler.of(context).config;
    final userDataProvider = config.useMockProvider ? MockUserProvider() : RemoteUserProvider();

    // ChangeNotifierProvider UserListViewModel is needed by UserList
    return ChangeNotifierProvider<UserListViewModel>(
      create: (context) => UserListViewModel(UserRepository(userDataProvider)),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Lycium users"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Expanded(child: UserList()),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';

/// Configuration object to dispatch runtime constants.
/// Should be used with ConfigHandler object to have it
/// available as an InheritedWidget
class Config {
  static Config defaultConfiguration = Config();
  static Config testConfiguration = Config(true, false);

  Config([
    this.useMockProvider = false,
    this.allowNetwork = true,
  ]);

  bool useMockProvider;
  bool allowNetwork;
}

class ConfigHandler extends InheritedWidget {
  final Config config;

  const ConfigHandler({
    Key? key,
    required this.config,
    required Widget child,
  }) : super(key: key, child: child);

  static ConfigHandler of(BuildContext context) {
    final ConfigHandler? result = context.dependOnInheritedWidgetOfExactType<ConfigHandler>();
    assert(result != null, 'No ConfigHandler found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(covariant ConfigHandler oldWidget) => oldWidget.config != config;
}

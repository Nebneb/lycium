# lycium

A demo flutter application.

## Getting Started

This project is a demo project using [RandomUser API](https://randomuser.me/).

External dependencies are :

- provider: 6.0.0
- http: 0.13.4
- url_launcher: 6.0.12

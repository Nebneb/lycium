// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';
import 'package:lycium/config.dart';
import 'package:lycium/data/mock_user_provider.dart';
import 'package:lycium/main.dart';
import 'package:lycium/user/user_view_model.dart';

void main() {
  testWidgets('App startup', (WidgetTester tester) async {
    await tester.pumpWidget(MainApp(Config.testConfiguration));
    expect(find.text("Lycium users"), findsOneWidget);
  });

  testWidgets('Test that some users are loaded', (WidgetTester tester) async {
    // Start the app
    await tester.pumpWidget(MainApp(Config.testConfiguration));
    await tester.pumpAndSettle(const Duration(milliseconds: 1000));
    // Get a mock user
    final mockUser = MockUserProvider().getRandomUser();
    final mockUserViewModel = UserViewModel(mockUser);
    // Check that its name is in the list
    expect(find.text(mockUserViewModel.fullName), findsOneWidget);
  });

  testWidgets('Test user profile', (WidgetTester tester) async {
    // Start the app
    await tester.pumpWidget(MainApp(Config.testConfiguration));
    await tester.pumpAndSettle(const Duration(milliseconds: 1000));
    // Get a mock user
    final mockUser = MockUserProvider().getRandomUser();
    final mockUserViewModel = UserViewModel(mockUser);
    // Find it in the list and tap on the row
    var userRow = find.text(mockUserViewModel.fullName);
    await tester.tap(userRow);
    // Wait for the next page
    await tester.pumpAndSettle(const Duration(milliseconds: 1000));
    expect(find.text(mockUserViewModel.fullName), findsOneWidget);
    expect(find.textContaining(mockUserViewModel.email), findsOneWidget);
  });

}
